// Copyright IBM Corp. 2018,2019. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {Getter, inject} from '@loopback/core';
import {
  DefaultCrudRepository,
  BelongsToAccessor,
  HasManyRepositoryFactory,
  juggler,
  repository,
  DataObject,
  Options
} from '@loopback/repository';
import {FlowRepository, InstancePortRepository} from '../repositories'
import {Flow, Instance, InstancePortWithRelations, InstanceRelations} from '../models';
import * as _ from 'lodash';
import {NginxProxyServiceBindings, WorkflowProcessesServiceBindings} from "../keys";
import {NginxProxy, WorkflowProcesses} from "../classes";
import {FreePorts} from '../classes';
// import getPort, {portNumbers} from 'get-port';
// const getPort = await import('get-port');
import {ConfigLoaderBindings} from "@loopback4/config-loader";
import {ConfigProvider} from "@loopback4/config-loader/dist/configProvider";


export class InstanceRepository extends DefaultCrudRepository<
  Instance,
  typeof Instance.prototype.id,
  InstanceRelations
> {
  public readonly ports: HasManyRepositoryFactory<InstancePortWithRelations, typeof Instance.prototype.id>;
  public readonly flow: BelongsToAccessor<Flow, typeof Instance.prototype.id>;
  private freePorts: FreePorts = new FreePorts({});
  constructor(
      @inject('datasources.db') dataSource: juggler.DataSource,
      @inject(ConfigLoaderBindings.CONFIG_LOADER) private configLoader: ConfigProvider,
      @inject(NginxProxyServiceBindings.NGINX_PROXY_SERVICE) private nginxProxy: NginxProxy,
      @inject(WorkflowProcessesServiceBindings.WORKFLOW_PROCESSES_SERVICE) private workflowProcesses: WorkflowProcesses,
      @repository.getter('InstancePortRepository') protected instancePortRepositoryGetter: Getter<InstancePortRepository>,
      @repository.getter('FlowRepository') protected flowRepositoryGetter: Getter<FlowRepository>,
      @repository(FlowRepository) protected flowRepository: FlowRepository
  ) {
    super(Instance, dataSource);
    this.ports = this.createHasManyRepositoryFactoryFor('ports', instancePortRepositoryGetter);
    this.registerInclusionResolver('ports', this.ports.inclusionResolver);
    this.flow = this.createBelongsToAccessorFor('flow', flowRepositoryGetter);
    this.registerInclusionResolver('flow', this.flow.inclusionResolver);
  }

  checkGlobals(flow: Flow, globals: any) {
    let missing: string[] = [];
    if (flow.requiredGlobals) {
      for (const reqGlob of flow.requiredGlobals) {
        if (globals && (globals as any)[reqGlob] === undefined) {
          missing.push(reqGlob);
        }
      }
    }
    if (missing.length !== 0) {
      return missing;
    }
    return true;
  }

  async getManagementPorts(): Promise<number[]> {
    return (
        await this.find({fields: {managementPort: true}})
    ).map((instance) => {return instance.managementPort}) as number[];
  }

  async create(entity: DataObject<Instance>, options?: Options): Promise<Instance> {
    const flow = await this.flowRepository.findById(entity.flowId);
    const check = this.checkGlobals(flow, entity.globals);
    if (check === true) {
      const config = this.configLoader.config;
      console.log('Flow Agent Config', config.flowAgent);
      // entity.managementPort = await getPort.default({port: getPort.portNumbers(config.flowAgent.managementPortStart, config.flowAgent.managementPortEnd), exclude: await this.getManagementPorts()});
      this.freePorts.update({
        excludeTcp: await this.getManagementPorts(),
        ports: _.range(config.flowAgent.managementPortStart, config.flowAgent.managementPortEnd),
      });
      entity.managementPort = await this.freePorts.get('tcp');
      const instance = await super.create(entity, options);
      await this.nginxProxy.createInstance(instance);
      await this.workflowProcesses.createInstance(instance);
      return instance;
    }
    else {
      throw new Error(`Missing Globals(${_.join(check, ',')})`);
    }
  }

  async updateById(id: typeof Instance.prototype.id, data: DataObject<Instance>, options?: Options): Promise<void> {
    if (data.globals) {
      const instance = await this.findById(id);
      const flow = await this.flowRepository.findById(instance.flowId);
      const check = this.checkGlobals(flow, data.globals);
      if (check !== true) {
        throw new Error(`Missing Globals(${_.join(check, ',')})`);
      }
    }
    await this.nginxProxy.updateInstanceById(id, data);
    await this.workflowProcesses.updateInstanceById(id, data);
    return super.updateById(id, data, options);
  }
  async replaceById(id: typeof Instance.prototype.id, data: DataObject<Instance>, options?: Options): Promise<void> {
    if (data.globals) {
      const instance = await this.findById(id);
      const flow = await this.flowRepository.findById(instance.flowId);
      const check = this.checkGlobals(flow, data.globals);
      if (check !== true) {
        throw new Error(`Missing Globals(${_.join(check, ',')})`);
      }
    }
    await this.nginxProxy.replaceInstanceById(id, data);
    await this.workflowProcesses.replaceInstanceById(id, data);
    return super.replaceById(id, data, options);
  }

  async delete(entity: Instance, options?: any): Promise<void> {
    await this.nginxProxy.deleteInstance(entity);
    await this.workflowProcesses.deleteInstance(entity);
    await this.ports(entity.id).delete({});
    return super.delete(entity, options);
  }
  async deleteById(id: typeof Instance.prototype.id, options?: any): Promise<void> {
    await this.nginxProxy.deleteInstanceById(id);
    await this.workflowProcesses.deleteInstanceById(id);
    await this.ports(id).delete({});
    return super.deleteById(id, options);
  }
}
