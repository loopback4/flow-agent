// Copyright IBM Corp. 2018,2019. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  juggler,
  repository,
  DataObject,
  Options
} from '@loopback/repository';
import {InstancePort, InstancePortRelations, InstanceWithRelations} from '../models';
import {InstanceRepository} from "../repositories";
import {NginxProxyServiceBindings, WorkflowProcessesServiceBindings} from "../keys";
import {NginxProxy, WorkflowProcesses} from "../classes";

export class InstancePortRepository extends DefaultCrudRepository<
  InstancePort,
  typeof InstancePort.prototype.id,
  InstancePortRelations
> {
  public readonly instance: BelongsToAccessor<InstanceWithRelations, typeof InstancePort.prototype.id>;
  constructor(
      @inject('datasources.db') dataSource: juggler.DataSource,
      @inject(NginxProxyServiceBindings.NGINX_PROXY_SERVICE) private nginxProxy: NginxProxy,
      @inject(WorkflowProcessesServiceBindings.WORKFLOW_PROCESSES_SERVICE) private workflowProcesses: WorkflowProcesses,
      @repository.getter('InstanceRepository') protected instanceRepositoryGetter: Getter<InstanceRepository>
  ) {
    super(InstancePort, dataSource);
    this.instance = this.createBelongsToAccessorFor('instance', instanceRepositoryGetter);
    this.registerInclusionResolver('instance', this.instance.inclusionResolver);
  }
  async create(entity: DataObject<InstancePort>, options?: Options): Promise<InstancePort> {
    const instancePort = await super.create(entity, options);
    await this.nginxProxy.createInstancePort(instancePort);
    await this.workflowProcesses.createInstancePort(instancePort);
    return instancePort;
  }

  async updateById(id: typeof InstancePort.prototype.id, data: DataObject<InstancePort>, options?: Options): Promise<void> {
    await this.nginxProxy.updateInstancePortById(id, data);
    await this.workflowProcesses.updateInstancePortById(id, data);
    return super.updateById(id, data, options);
  }
  async replaceById(id: typeof InstancePort.prototype.id, data: DataObject<InstancePort>, options?: Options): Promise<void> {
    await this.nginxProxy.replaceInstancePortById(id, data);
    await this.workflowProcesses.replaceInstancePortById(id, data);
    return super.replaceById(id, data, options);
  }

  async delete(entity: InstancePort, options?: Options): Promise<void> {
    await this.nginxProxy.deleteInstancePort(entity);
    await this.workflowProcesses.deleteInstancePort(entity);
    return super.delete(entity, options);
  }
  async deleteById(id: typeof InstancePort.prototype.id, options?: Options): Promise<void> {
    await this.nginxProxy.deleteInstancePortById(id);
    await this.workflowProcesses.deleteInstancePortById(id);
    return super.deleteById(id, options);
  }
}
