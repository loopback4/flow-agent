// Copyright IBM Corp. 2018,2019. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  juggler,
  repository,
  HasManyRepositoryFactory,
  DataObject,
  Options
} from '@loopback/repository';
import { InstanceRepository } from '../repositories'
import {Flow, FlowRelations, Instance, InstanceWithRelations} from '../models';
import {NginxProxyServiceBindings, WorkflowProcessesServiceBindings} from "../keys";
import {NginxProxy, WorkflowProcesses} from "../classes";

export class FlowRepository extends DefaultCrudRepository<
  Flow,
  typeof Flow.prototype.id,
  FlowRelations
> {
  public readonly instances: HasManyRepositoryFactory<Instance, typeof Flow.prototype.id>;
  constructor(
      @inject('datasources.db') dataSource: juggler.DataSource,
      @inject(NginxProxyServiceBindings.NGINX_PROXY_SERVICE) private nginxProxy: NginxProxy,
      @inject(WorkflowProcessesServiceBindings.WORKFLOW_PROCESSES_SERVICE) private workflowProcesses: WorkflowProcesses,
      @repository.getter('InstanceRepository') protected instanceRepositoryGetter: Getter<InstanceRepository>
  ) {
    super(Flow, dataSource);
    this.instances = this.createHasManyRepositoryFactoryFor('instances', instanceRepositoryGetter);
    this.registerInclusionResolver('instances', this.instances.inclusionResolver);
  }

  async create(entity: DataObject<Partial<Flow>>, options?: Options): Promise<Flow> {
    entity.requiredGlobals = [];
    if (entity?.config?.store_definition) {
      for (const storeDef of entity.config.store_definition) {
        if (storeDef?.required_for_init && storeDef?.name) {
          entity.requiredGlobals.push(storeDef.name);
        }
      }
    }
    if (!entity.name) {
      entity.name = entity.config?.name;
    }
    const flow = await super.create(entity, options);
    await this.nginxProxy.createFlow(flow);
    await this.workflowProcesses.createFlow(flow);
    return flow;
  }

  processUpdateData(data: DataObject<Flow>) {
    if (data.config) {
      data.requiredGlobals = [];
      if (!data.name) {
        data.name = data.config.name;
      }
      if (data?.config?.store_definition) {
        for (const storeDef of data.config.store_definition) {
          if (storeDef?.required_for_init && storeDef?.name) {
            data.requiredGlobals.push(storeDef.name);
          }
        }
      }
    }
  }
  async updateById(id: typeof Flow.prototype.id, data: DataObject<Flow>, options?: Options): Promise<void> {
    this.processUpdateData(data);
    await this.nginxProxy.updateFlowById(id, data);
    return super.updateById(id, data, options);
  }
  async replaceById(id: typeof Flow.prototype.id, data: DataObject<Flow>, options?: Options): Promise<void> {
    this.processUpdateData(data);
    await this.nginxProxy.replaceFlowById(id, data);
    await this.workflowProcesses.replaceFlowById(id, data);
    return super.replaceById(id, data, options);
  }

  async delete(entity: Flow, options?: any): Promise<void> {
    await this.nginxProxy.deleteFlow(entity);
    await this.workflowProcesses.deleteFlow(entity);
    await this.instances(entity.id).delete();
    return super.delete(entity, options);
  }
  async deleteById(id: typeof Flow.prototype.id, options?: any): Promise<void> {
    await this.nginxProxy.deleteFlowById(id);
    await this.workflowProcesses.deleteFlowById(id);
    await this.instances(id).delete({});
    return super.deleteById(id, options);
  }
}
