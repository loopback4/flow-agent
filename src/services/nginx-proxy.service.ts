import { Provider, ValueOrPromise } from '@loopback/context';
import {NginxProxy} from "../classes";
import {BindingScope, CoreBindings, inject, injectable} from "@loopback/core";
import {WebsocketApplication} from "@loopback4/websocket";

const nginxProxy = new NginxProxy();

@injectable({scope: BindingScope.SINGLETON})
export class NginxProxyService implements Provider<NginxProxy>{
    private initialized: boolean = false;
    constructor(
        @inject(CoreBindings.APPLICATION_INSTANCE) private application: WebsocketApplication,
    ) {
    }

    async value(): Promise<NginxProxy> {
        if (!this.initialized) {
            await nginxProxy.init(this.application);
        }
        return nginxProxy;
    }

}
