import { Provider, ValueOrPromise } from '@loopback/context';
import {WorkflowProcesses} from "../classes";
import {BindingScope, CoreBindings, inject, injectable} from "@loopback/core";
import {WebsocketApplication} from "@loopback4/websocket";

const workflowProcesses = new WorkflowProcesses();

@injectable({scope: BindingScope.SINGLETON})
export class WorkflowProcessesService implements Provider<WorkflowProcesses>{
    private initialized: boolean = false;
    constructor(
        @inject(CoreBindings.APPLICATION_INSTANCE) private application: WebsocketApplication,
    ) {
    }

    async value(): Promise<WorkflowProcesses> {
        if (!this.initialized) {
            await workflowProcesses.init(this.application);
        }
        return workflowProcesses;
    }

}
