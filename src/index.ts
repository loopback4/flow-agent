import {WebsocketApplication} from "@loopback4/websocket";
import {Context, Component, CoreBindings, inject, Binding } from '@loopback/core';
import {Flow, Instance, InstancePort} from "./models";
import {FlowRepository, InstanceRepository, InstancePortRepository} from "./repositories";
import {FlowController, InstanceController, InstancePortController} from "./controllers";
import {WorkflowProcessesServiceBindings, NginxProxyServiceBindings, RepositoryConstants} from "./keys";
import {WorkflowProcessesService, NginxProxyService} from "./services";
// import {repository} from "@loopback/repository";
// import * as _ from 'lodash';
export * from './keys';

export class FlowAgentComponent implements Component {
    controllers = [FlowController, InstanceController, InstancePortController];
    models = [Flow, Instance, InstancePort];
    // repositories = [FlowRepository, InstanceRepository, InstancePortRepository];
    // providers = {
    //     'flow-agent': LoggerProvider
    // };
    bindings: Binding[] = [
        // @ts-ignore
        Binding.bind(NginxProxyServiceBindings.NGINX_PROXY_SERVICE).toClass(NginxProxyService),
        Binding.bind(WorkflowProcessesServiceBindings.WORKFLOW_PROCESSES_SERVICE).toClass(WorkflowProcessesService),

        Binding.bind(RepositoryConstants.FLOW_REPOSITORY).toClass(FlowRepository),
        Binding.bind(RepositoryConstants.INSTANCE_REPOSITORY).toClass(InstanceRepository),
        Binding.bind(RepositoryConstants.INSTANCE_PORT_REPOSITORY).toClass(InstancePortRepository),
    ];
    constructor(
        // @inject(NginxProxyServiceBindings.NGINX_PROXY_SERVICE) private nginxProxyService: NginxProxyService,
        // @repository(FlowRepository) protected flowRepository: FlowRepository,
        // @repository(InstanceRepository) protected instanceRepository: InstanceRepository,
        @inject(CoreBindings.APPLICATION_INSTANCE) private application: WebsocketApplication,
    ) {
        application.onStart(async () => {
            const nginxProxy = await application.getBinding(NginxProxyServiceBindings.NGINX_PROXY_SERVICE).getValue(application);
            nginxProxy.value();
            const workflowProcesses = await application.getBinding(WorkflowProcessesServiceBindings.WORKFLOW_PROCESSES_SERVICE).getValue(application);
            nginxProxy.value();
        //     let instances = _.groupBy(
        //         await this.instanceRepository.find({where: {running: true},include:[{relation: 'ports'}]}),
        //         (instance) => { return instance.flowId }
        //     );
        //     let flowIds = _.keys(instances).map((i) => { return parseInt(i); });
        //     let flows = await this.flowRepository.find({where:{id: {inq: flowIds}}});
        //     console.log('Flows:', flows);
        });
    }

}
