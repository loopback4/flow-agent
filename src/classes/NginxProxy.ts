import {WebsocketApplication} from "@loopback4/websocket";
import {RepositoryConstants} from "../keys";
import {InstanceRepository} from "../repositories";
import {Flow, Instance, InstancePort, InstanceWithRelations} from "../models";
import * as _ from 'lodash';
import {DataObject} from "@loopback/repository";
import {ConfigLoaderBindings} from "@loopback4/config-loader";
import {LoggerServiceBindings} from "@loopback4/logger";

export class NginxProxy {
    constructor() {
    }
    async init(app: WebsocketApplication) {
        console.log('Nginx Proxy Initialized');
        const logger = await app.getBinding(LoggerServiceBindings.LOGGER_SERVICE).getValue(app);
        console.log(logger);
        const loader = await app.getBinding(ConfigLoaderBindings.CONFIG_LOADER).getValue(app);
        const config = loader.config;
        console.log('Config', config);
        const instanceRepository: InstanceRepository = await app.getBinding(RepositoryConstants.INSTANCE_REPOSITORY).getValue(app);
        for (const instance of await instanceRepository.find({where: {persist: false}})) {
            console.log(`Deleting non-persistent instance(${instance.id})`);
            await instanceRepository.deleteById(instance.id);
        }
        for (const instance of await instanceRepository.find({where: {running: true},include:[{relation: 'ports'}]})) {
            // TODO Handle generating nginx config and reloading
        }
    }

    // Flow intercept calls
    async createFlow(flow: Flow) {
    }
    async updateFlowById(id: typeof Flow.prototype.id, data: DataObject<Flow>) {
    }
    async replaceFlowById(id: typeof Flow.prototype.id, data: DataObject<Flow>) {
    }
    async deleteFlow(flow: Flow) {
    }
    async deleteFlowById(id: typeof Flow.prototype.id) {
    }

    // Instance intercept calls
    async createInstance(instance: Instance) {
    }
    async updateInstanceById(id: typeof Instance.prototype.id, data: DataObject<Instance>) {
    }
    async replaceInstanceById(id: typeof Instance.prototype.id, data: DataObject<Instance>) {
    }
    async deleteInstance(instance: Instance) {
    }
    async deleteInstanceById(id: typeof Instance.prototype.id) {
    }

    // InstancePort intercept calls
    async createInstancePort(instancePort: InstancePort) {
    }
    async updateInstancePortById(id: typeof InstancePort.prototype.id, data: DataObject<InstancePort>) {
    }
    async replaceInstancePortById(id: typeof InstancePort.prototype.id, data: DataObject<InstancePort>) {
    }
    async deleteInstancePort(instancePort: InstancePort) {
    }
    async deleteInstancePortById(id: typeof InstancePort.prototype.id) {
    }
}
