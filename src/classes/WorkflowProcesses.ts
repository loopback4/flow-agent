import {WebsocketApplication} from "@loopback4/websocket";
import {Flow, Instance, InstancePort} from "../models";
import {DataObject} from "@loopback/repository";
import {ConfigLoaderBindings} from "@loopback4/config-loader";
import {FlowRepository, InstanceRepository} from "../repositories";
import {RepositoryConstants} from "../keys";
import * as _ from "lodash";

export class WorkflowProcesses {
    constructor() {
    }
    async init(app: WebsocketApplication) {
        console.log('Workflow Processes Initialized');
        const loader = await app.getBinding(ConfigLoaderBindings.CONFIG_LOADER).getValue(app);
        const config = loader.config;
        const flowRepository: FlowRepository = await app.getBinding(RepositoryConstants.FLOW_REPOSITORY).getValue(app);
        const instanceRepository: InstanceRepository = await app.getBinding(RepositoryConstants.INSTANCE_REPOSITORY).getValue(app);
        for (const instance of await instanceRepository.find({where: {persist: false}})) {
            await instanceRepository.deleteById(instance.id);
        }
        // instances = await instanceRepository.find({where: {running: true}});

        let groupedInstances = _.groupBy(
            await instanceRepository.find({where: {running: true},include:[{relation: 'ports'}]}),
            (instance) => { return instance.flowId }
        );
        let flowIds = _.keys(groupedInstances).map((i) => { return parseInt(i); });
        let flows = await flowRepository.find({where:{id: {inq: flowIds}}});
        for (const flow of flows) {
            if (flow.id) {
                let flowInstances = groupedInstances[flow.id];
                for (const instance of flowInstances) {
                    // TODO Handle processing of workflow processes
                }
            }
        }
    }

    // Flow intercept calls
    async createFlow(flow: Flow) {
    }
    async updateFlowById(id: typeof Flow.prototype.id, data: DataObject<Flow>) {
    }
    async replaceFlowById(id: typeof Flow.prototype.id, data: DataObject<Flow>) {
    }
    async deleteFlow(flow: Flow) {
    }
    async deleteFlowById(id: typeof Flow.prototype.id) {
    }

    // Instance intercept calls
    async createInstance(instance: Instance) {
    }
    async updateInstanceById(id: typeof Instance.prototype.id, data: DataObject<Instance>) {
    }
    async replaceInstanceById(id: typeof Instance.prototype.id, data: DataObject<Instance>) {
    }
    async deleteInstance(instance: Instance) {
    }
    async deleteInstanceById(id: typeof Instance.prototype.id) {
    }

    // InstancePort intercept calls
    async createInstancePort(instancePort: InstancePort) {
    }
    async updateInstancePortById(id: typeof InstancePort.prototype.id, data: DataObject<InstancePort>) {
    }
    async replaceInstancePortById(id: typeof InstancePort.prototype.id, data: DataObject<InstancePort>) {
    }
    async deleteInstancePort(instancePort: InstancePort) {
    }
    async deleteInstancePortById(id: typeof InstancePort.prototype.id) {
    }
}
