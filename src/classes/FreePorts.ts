import * as _ from 'lodash';
import * as dgram from 'dgram';
import * as net from 'net';
import * as os from "os";

const minPort = 1024;
const maxPort = 65535;

export interface FreePortsOptions {
    ports?: number | number[],
    excludeTcp?: number | number[],
    excludeUdp?: number | number[],
    host?: string,
}
export type Protocol = "tcp" | "udp";

export class FreePorts {
    // private interfaces: NodeJS.Dict<os.NetworkInterfaceInfo[]>;
    // private addresses: Set<undefined | string> = new Set([undefined, '0.0.0.0']);
    public host?: string;
    public ports: number[] = _.range(minPort, maxPort);
    public excludedTcpPorts?: number[] = [];
    public excludedUdpPorts?: number[] = [];
    constructor(private options?: FreePortsOptions) {
        this.update(options);
    }
    update(options?: FreePortsOptions) {
        if (options) {
            this.host = options.host;
            if (options.excludeTcp && !Array.isArray(options.excludeTcp)) {
                this.excludedTcpPorts = [options.excludeTcp];
            }
            else if (options.excludeTcp && Array.isArray(options.excludeTcp)) {
                this.excludedTcpPorts = options.excludeTcp;
            }
            if (options.excludeUdp && !Array.isArray(options.excludeUdp)) {
                this.excludedUdpPorts = [options.excludeUdp];
            }
            else if (options.excludeUdp && Array.isArray(options.excludeUdp)) {
                this.excludedUdpPorts = options.excludeUdp;
            }
            if (options.ports && !Array.isArray(options.ports)) { this.ports = [options.ports]; }
            else if (options.ports && Array.isArray(options.ports)) { this.ports = options.ports; }
            else { this.ports = _.range(minPort, maxPort); }
        }
        else { this.ports = _.range(minPort, maxPort); }
        // this.interfaces = os.networkInterfaces();
        // this.getAddresses();
    }

    async get(protocol: Protocol = "tcp", host?: string) {
        for (const port of this.ports) {
            if (!_.includes(this.excludedTcpPorts, port) && await this.checkPort(protocol, port, host)) {
                return port;
            }
        }
        return undefined;
    }

    // private getAddresses() {
    //     // Undefined value for createServer to use default host
    //     // default IPv4 host in case createServer default to IPv6
    //     const results = new Set([undefined, '0.0.0.0']);
    //     for (const iface of Object.values(this.interfaces)) {
    //         if (iface) {
    //             for (const config of iface) {
    //                 results.add(config.address);
    //             }
    //         }
    //     }
    //     this.addresses = results;
    //     return results;
    // }
    async checkPort(protocol: Protocol, port: number, host?: string) {
        if (protocol === 'tcp') {
            return new Promise((resolve, reject) => {
                const server = net.createServer();
                server.unref();
                server.on('error', reject);
                server.listen(port, host, () => {
                    server.close(() => {
                        resolve(port);
                    });
                });
            });
        }
        else if (protocol === 'udp') {
            return new Promise((resolve, reject) => {
                const socket = dgram.createSocket('udp4');
                socket.on('error', reject)
                socket.bind(port, host, () => {
                    socket.close();
                    resolve(port);
                })
            });
        }
        else {
            return undefined;
        }
    }
}
