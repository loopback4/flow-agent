import {Entity, model, property} from "@loopback/repository";
import {WorkflowConfigStoreDefinition} from "./workflow.store_definition.model";


@model()
export class WorkflowConfig extends Entity {
    @property({
        type: 'string'
    })
    name: string;

    @property({
        type: 'any'
    })
    nodes: any[]

    @property({
        type: 'array',
        itemType: 'object'
    })
    store_definition: WorkflowConfigStoreDefinition[];
}
