import {Entity, model, property, belongsTo, hasMany} from '@loopback/repository';
import {Instance, InstanceWithRelations} from "./instance.model";

@model()
export class InstancePort extends Entity {
    @property({
        type: 'number',
        id: true,
        generated: false,
    })
    id?: number;

    @property({
        type: 'number',
        required: true,
    })
    publicPort: number;

    @property({
        type: 'number',
    })
    localPort?: number;

    @property({
        type: 'string',
    })
    node: string;

    @property({
        type: 'string',
    })
    protocol?: string; // address,city,zipcode

    @property({
        type: 'string',
    })
    url?: string; // latitude,longitude

    @belongsTo(() => Instance)
    instanceId: number;

    constructor(data?: Partial<InstancePort>) {
        super(data);
    }
}

export interface InstancePortRelations {
    instance?: InstanceWithRelations
}

export type InstancePortWithRelations = InstancePort & InstancePortRelations;
