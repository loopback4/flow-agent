// Copyright IBM Corp. 2018,2020. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {Entity, model, property, hasMany} from '@loopback/repository';
import {Instance, InstanceWithRelations} from './instance.model';
import {WorkflowConfig} from './workflow.model';

@model()
export class Flow extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: false,
  })
  id?: number;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'object',
    required: true,
  })
  config: WorkflowConfig;

  @property({
    type: 'array',
    itemType: 'string'
  })
  requiredGlobals?: string[];

  @hasMany(() => Instance)
  instances?: Instance[];

  constructor(data?: Partial<Flow>) {
    super(data);
  }
}

export interface FlowRelations {
  instances?: InstanceWithRelations
}

export type FlowWithRelations = Flow & FlowRelations;
