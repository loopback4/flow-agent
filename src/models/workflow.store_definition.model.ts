import {Entity, model, property} from "@loopback/repository";


@model()
export class WorkflowConfigStoreDefinition extends Entity {
    @property({
        type: 'string'
    })
    name: string;

    @property({
        type: 'string'
    })
    type: string;

    @property({
        type: 'any'
    })
    init_value: any;

    @property({
        type: 'boolean'
    })
    allow_null: boolean;

    @property({
        type: 'boolean'
    })
    required: boolean;

    @property({
        type: 'boolean'
    })
    required_for_init: boolean;
}
