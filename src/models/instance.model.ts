import {Entity, model, property, belongsTo, hasMany} from '@loopback/repository';
import {Flow, FlowWithRelations} from "./flow.model";
import {InstancePort} from "./instance-port.model";

@model()
export class Instance extends Entity {
    @property({
        type: 'number',
        id: true,
        generated: false,
    })
    id?: number;

    @property({
        type: 'string',
        required: true,
    })
    name: string;

    @property({
        type: 'object',
    })
    globals?: Object;

    @property({
        type: 'boolean',
        default: false
    })
    persist?: boolean;

    @property({
        type: 'number',
    })
    managementPort?: number; // address,city,zipcode

    @property({
        type: 'boolean',
        default: false
    })
    running?: boolean; // latitude,longitude

    @belongsTo(() => Flow)
    flowId: number;

    @hasMany(() => InstancePort)
    ports?: InstancePort[];

    constructor(data?: Partial<Instance>) {
        super(data);
    }
}

export interface InstanceRelations {
    flow?: FlowWithRelations
}

export type InstanceWithRelations = Instance & InstanceRelations;
