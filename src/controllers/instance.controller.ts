// Copyright IBM Corp. 2018,2020. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { inject } from '@loopback/core';
import { Filter, repository } from '@loopback/repository';
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody, HttpErrors} from '@loopback/rest';
import {Instance} from '../models';
import {InstanceRepository} from '../repositories';

export class InstanceController {
  constructor(
    @repository(InstanceRepository) protected instanceRepository: InstanceRepository,
  ) {
  }

  @post('/instances', {
    responses: {
      '200': {
        description: 'Instance model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Instance) } },
      },
    },
  })
  async createInstance(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Instance, { title: 'NewInstance', exclude: ['id', 'managementPort'] }),
        },
      },
    })
      instance: Omit<Instance, 'id'>,
  ): Promise<Instance>
  {
    try {
      if (instance.managementPort) {
        delete instance.managementPort;
      }
      return await this.instanceRepository.create(instance);
    } catch (error) {
      throw new HttpErrors.UnprocessableEntity(error.toString());
    }
  }

  @get('/instances/{id}', {
    responses: {
      '200': {
        description: 'Instance model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Instance) } },
      },
    },
  })
  async findInstanceById(
    @param.path.number('id') id: number,
    @param.query.boolean('items') items?: boolean,
  ): Promise<Instance|undefined>
  {
    return this.instanceRepository.findById(id);
  }

  @get('/instances', {
    responses: {
      '200': {
        description: 'Array of Instance model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Instance) },
          },
        },
      },
    },
  })
  async findInstances(
    @param.filter(Instance)
      filter?: Filter<Instance>,
  ): Promise<Instance[]|undefined>
  {
    return this.instanceRepository.find(filter);
  }

  @put('/instances/{id}', {
    responses: {
      '204': {
        description: 'Instance PUT success',
      },
    },
  })
  async replaceInstance(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Instance, {
            partial: true,
            exclude: ['id', 'flowId', 'managementPort']
          }),
        },
      },
    }) instance: Omit<Omit<Instance, 'id'>, 'flowId'>,
  ): Promise<void>
  {
    if (instance.managementPort) {
      delete instance.managementPort;
    }
    await this.instanceRepository.replaceById(id, instance);
  }

  @patch('/instances/{id}', {
    responses: {
      '204': {
        description: 'Instance PATCH success',
      },
    },
  })
  async updateInstance(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Instance, {
            partial: true,
            exclude: ['id', 'flowId', 'managementPort']
          }),
        },
      },
    })
      instance: Partial<Instance>,
  ): Promise<void>
  {
    await this.instanceRepository.updateById(id, instance);
  }

  @del('/instances/{id}', {
    responses: {
      '204': {
        description: 'Instance DELETE success',
      },
    },
  })
  async deleteInstance(
    @param.path.number('id') id: number
  ): Promise<void>
  {
    await this.instanceRepository.deleteById(id);
  }
}
