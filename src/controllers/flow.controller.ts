// Copyright IBM Corp. 2018,2020. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { inject } from '@loopback/core';
import { Filter, repository } from '@loopback/repository';
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody, } from '@loopback/rest';
import { Flow } from '../models';
import { FlowRepository } from '../repositories';

export class FlowController {
  constructor(
    @repository(FlowRepository) protected flowRepository: FlowRepository,
  ) {
  }

  @post('/flows', {
    responses: {
      '200': {
        description: 'Flow model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Flow) } },
      },
    },
  })
  async createFlow(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Flow, {title: 'NewFlow', exclude: ['id', 'name', 'requiredGlobals', 'instances'] }),
        },
      },
    })
      flow: Omit<Flow, 'id' | 'name' | 'requiredGlobals' | 'instances'>,
  ): Promise<Flow>
  {
    const created = await this.flowRepository.create(flow);
    if (created) {
      console.log('Created:', created);
    }
    return created;
  }

  @get('/flows/{id}', {
    responses: {
      '200': {
        description: 'Flow model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Flow) } },
      },
    },
  })
  async findFlowById(
    @param.path.number('id') id: number,
    @param.query.boolean('items') items?: boolean,
  ): Promise<Flow|undefined>
  {
    return this.flowRepository.findById(id);
  }

  @get('/flows', {
    responses: {
      '200': {
        description: 'Array of Flow model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Flow) },
          },
        },
      },
    },
  })
  async findFlows(
    @param.filter(Flow)
      filter?: Filter<Flow>,
  ): Promise<Flow[]|undefined>
  {
    return this.flowRepository.find(filter);
  }

  @put('/flows/{id}', {
    responses: {
      '204': {
        description: 'Flow PUT success',
      },
    },
  })
  async replaceFlow(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Flow, {
            partial: true,
            exclude: ['id', 'name', 'requiredGlobals']
          }),
        },
      },
    }) flow: Omit<Flow, 'id'>,
  ): Promise<void>
  {
    if (flow.name) {
      delete flow.name;
    }
    if (flow.requiredGlobals) {
      delete flow.requiredGlobals;
    }
    await this.flowRepository.replaceById(id, flow);
  }

  @patch('/flows/{id}', {
    responses: {
      '204': {
        description: 'Flow PATCH success',
      },
    },
  })
  async updateFlow(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Flow, {
            partial: true,
            exclude: ['id', 'name', 'requiredGlobals']
          }),
        },
      },
    })
      flow: Partial<Flow>,
  ): Promise<void>
  {
    if (flow.name) {
      delete flow.name;
    }
    if (flow.requiredGlobals) {
      delete flow.requiredGlobals;
    }
    await this.flowRepository.updateById(id, flow);
  }

  @del('/flows/{id}', {
    responses: {
      '204': {
        description: 'Flow DELETE success',
      },
    },
  })
  async deleteFlow(
    @param.path.number('id') id: number
  ): Promise<void>
  {
    await this.flowRepository.deleteById(id);
  }
}
