// Copyright IBM Corp. 2018,2020. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { inject } from '@loopback/core';
import { Filter, repository } from '@loopback/repository';
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody, } from '@loopback/rest';
import { InstancePort } from '../models';
import { InstancePortRepository } from '../repositories';

export class InstancePortController {
  constructor(
    @repository(InstancePortRepository) protected portRepository: InstancePortRepository,
  ) {
  }

  @post('/ports', {
    responses: {
      '200': {
        description: 'InstancePort model port',
        content: { 'application/json': { schema: getModelSchemaRef(InstancePort) } },
      },
    },
  })
  async createInstancePort(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(InstancePort, { title: 'NewInstancePort', exclude: ['id'] }),
        },
      },
    })
      port: Omit<InstancePort, 'id'>,
  ): Promise<InstancePort>
  {
    return this.portRepository.create(port);
  }

  @get('/ports/{id}', {
    responses: {
      '200': {
        description: 'InstancePort model port',
        content: { 'application/json': { schema: getModelSchemaRef(InstancePort) } },
      },
    },
  })
  async findInstancePortById(
    @param.path.number('id') id: number,
    @param.query.boolean('items') items?: boolean,
  ): Promise<InstancePort|undefined>
  {
    return this.portRepository.findById(id);
  }

  @get('/ports', {
    responses: {
      '200': {
        description: 'Array of InstancePort model ports',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(InstancePort) },
          },
        },
      },
    },
  })
  async findInstancePorts(
    @param.filter(InstancePort)
      filter?: Filter<InstancePort>,
  ): Promise<InstancePort[]|undefined>
  {
    return this.portRepository.find(filter);
  }

  @put('/ports/{id}', {
    responses: {
      '204': {
        description: 'InstancePort PUT success',
      },
    },
  })
  async replaceInstancePort(
    @param.path.number('id') id: number,
    @requestBody() port: InstancePort,
  ): Promise<void>
  {
    await this.portRepository.replaceById(id, port);
  }

  @patch('/ports/{id}', {
    responses: {
      '204': {
        description: 'InstancePort PATCH success',
      },
    },
  })
  async updateInstancePort(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(InstancePort, { partial: true }),
        },
      },
    })
      port: Partial<InstancePort>,
  ): Promise<void>
  {
    await this.portRepository.updateById(id, port);
  }

  @del('/ports/{id}', {
    responses: {
      '204': {
        description: 'InstancePort DELETE success',
      },
    },
  })
  async deleteInstancePort(
    @param.path.number('id') id: number
  ): Promise<void>
  {
    await this.portRepository.deleteById(id);
  }
}
