import {BindingKey} from '@loopback/core';
import {WorkflowProcessesService} from "./services";

export namespace WorkflowProcessesServiceBindings {
    export const WORKFLOW_PROCESSES_SERVICE = BindingKey.create<WorkflowProcessesService>('services.flow-agent.workflow-processes');
}

export namespace RepositoryConstants {
    export const FLOW_REPOSITORY = 'repositories.FlowRepository';
    export const INSTANCE_REPOSITORY = 'repositories.InstanceRepository';
    export const INSTANCE_PORT_REPOSITORY = 'repositories.InstancePortRepository';
}
export namespace NginxProxyServiceBindings {
    export const NGINX_PROXY_SERVICE = BindingKey.create<WorkflowProcessesService>('services.flow-agent.nginx-proxy');
}
