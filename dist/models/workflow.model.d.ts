import { Entity } from "@loopback/repository";
import { WorkflowConfigStoreDefinition } from "./workflow.store_definition.model";
export declare class WorkflowConfig extends Entity {
    name: string;
    nodes: any[];
    store_definition: WorkflowConfigStoreDefinition[];
}
