"use strict";
// Copyright IBM Corp. 2018,2020. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.Flow = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
const instance_model_1 = require("./instance.model");
const workflow_model_1 = require("./workflow.model");
let Flow = class Flow extends repository_1.Entity {
    id;
    name;
    config;
    requiredGlobals;
    instances;
    constructor(data) {
        super(data);
    }
};
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'number',
        id: true,
        generated: false,
    }),
    tslib_1.__metadata("design:type", Number)
], Flow.prototype, "id", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], Flow.prototype, "name", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'object',
        required: true,
    }),
    tslib_1.__metadata("design:type", workflow_model_1.WorkflowConfig)
], Flow.prototype, "config", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'array',
        itemType: 'string'
    }),
    tslib_1.__metadata("design:type", Array)
], Flow.prototype, "requiredGlobals", void 0);
tslib_1.__decorate([
    (0, repository_1.hasMany)(() => instance_model_1.Instance),
    tslib_1.__metadata("design:type", Array)
], Flow.prototype, "instances", void 0);
Flow = tslib_1.__decorate([
    (0, repository_1.model)(),
    tslib_1.__metadata("design:paramtypes", [Object])
], Flow);
exports.Flow = Flow;
//# sourceMappingURL=flow.model.js.map