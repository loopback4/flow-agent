import { Entity } from '@loopback/repository';
import { InstanceWithRelations } from "./instance.model";
export declare class InstancePort extends Entity {
    id?: number;
    publicPort: number;
    localPort?: number;
    node: string;
    protocol?: string;
    url?: string;
    instanceId: number;
    constructor(data?: Partial<InstancePort>);
}
export interface InstancePortRelations {
    instance?: InstanceWithRelations;
}
export declare type InstancePortWithRelations = InstancePort & InstancePortRelations;
