"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InstancePort = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
const instance_model_1 = require("./instance.model");
let InstancePort = class InstancePort extends repository_1.Entity {
    id;
    publicPort;
    localPort;
    node;
    protocol; // address,city,zipcode
    url; // latitude,longitude
    instanceId;
    constructor(data) {
        super(data);
    }
};
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'number',
        id: true,
        generated: false,
    }),
    tslib_1.__metadata("design:type", Number)
], InstancePort.prototype, "id", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'number',
        required: true,
    }),
    tslib_1.__metadata("design:type", Number)
], InstancePort.prototype, "publicPort", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'number',
    }),
    tslib_1.__metadata("design:type", Number)
], InstancePort.prototype, "localPort", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], InstancePort.prototype, "node", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], InstancePort.prototype, "protocol", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], InstancePort.prototype, "url", void 0);
tslib_1.__decorate([
    (0, repository_1.belongsTo)(() => instance_model_1.Instance),
    tslib_1.__metadata("design:type", Number)
], InstancePort.prototype, "instanceId", void 0);
InstancePort = tslib_1.__decorate([
    (0, repository_1.model)(),
    tslib_1.__metadata("design:paramtypes", [Object])
], InstancePort);
exports.InstancePort = InstancePort;
//# sourceMappingURL=instance-port.model.js.map