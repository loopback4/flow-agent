import { Entity } from "@loopback/repository";
export declare class WorkflowConfigStoreDefinition extends Entity {
    name: string;
    type: string;
    init_value: any;
    allow_null: boolean;
    required: boolean;
    required_for_init: boolean;
}
