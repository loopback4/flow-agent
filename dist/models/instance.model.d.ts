import { Entity } from '@loopback/repository';
import { FlowWithRelations } from "./flow.model";
import { InstancePort } from "./instance-port.model";
export declare class Instance extends Entity {
    id?: number;
    name: string;
    globals?: Object;
    persist?: boolean;
    managementPort?: number;
    running?: boolean;
    flowId: number;
    ports?: InstancePort[];
    constructor(data?: Partial<Instance>);
}
export interface InstanceRelations {
    flow?: FlowWithRelations;
}
export declare type InstanceWithRelations = Instance & InstanceRelations;
