"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Instance = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
const flow_model_1 = require("./flow.model");
const instance_port_model_1 = require("./instance-port.model");
let Instance = class Instance extends repository_1.Entity {
    id;
    name;
    globals;
    persist;
    managementPort; // address,city,zipcode
    running; // latitude,longitude
    flowId;
    ports;
    constructor(data) {
        super(data);
    }
};
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'number',
        id: true,
        generated: false,
    }),
    tslib_1.__metadata("design:type", Number)
], Instance.prototype, "id", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Instance.prototype, "name", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'object',
    }),
    tslib_1.__metadata("design:type", Object)
], Instance.prototype, "globals", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'boolean',
        default: false
    }),
    tslib_1.__metadata("design:type", Boolean)
], Instance.prototype, "persist", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'number',
    }),
    tslib_1.__metadata("design:type", Number)
], Instance.prototype, "managementPort", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'boolean',
        default: false
    }),
    tslib_1.__metadata("design:type", Boolean)
], Instance.prototype, "running", void 0);
tslib_1.__decorate([
    (0, repository_1.belongsTo)(() => flow_model_1.Flow),
    tslib_1.__metadata("design:type", Number)
], Instance.prototype, "flowId", void 0);
tslib_1.__decorate([
    (0, repository_1.hasMany)(() => instance_port_model_1.InstancePort),
    tslib_1.__metadata("design:type", Array)
], Instance.prototype, "ports", void 0);
Instance = tslib_1.__decorate([
    (0, repository_1.model)(),
    tslib_1.__metadata("design:paramtypes", [Object])
], Instance);
exports.Instance = Instance;
//# sourceMappingURL=instance.model.js.map