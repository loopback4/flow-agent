import { Entity } from '@loopback/repository';
import { Instance, InstanceWithRelations } from './instance.model';
import { WorkflowConfig } from './workflow.model';
export declare class Flow extends Entity {
    id?: number;
    name?: string;
    config: WorkflowConfig;
    requiredGlobals?: string[];
    instances?: Instance[];
    constructor(data?: Partial<Flow>);
}
export interface FlowRelations {
    instances?: InstanceWithRelations;
}
export declare type FlowWithRelations = Flow & FlowRelations;
