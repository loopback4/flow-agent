"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkflowConfig = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
let WorkflowConfig = class WorkflowConfig extends repository_1.Entity {
    name;
    nodes;
    store_definition;
};
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string'
    }),
    tslib_1.__metadata("design:type", String)
], WorkflowConfig.prototype, "name", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'any'
    }),
    tslib_1.__metadata("design:type", Array)
], WorkflowConfig.prototype, "nodes", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'array',
        itemType: 'object'
    }),
    tslib_1.__metadata("design:type", Array)
], WorkflowConfig.prototype, "store_definition", void 0);
WorkflowConfig = tslib_1.__decorate([
    (0, repository_1.model)()
], WorkflowConfig);
exports.WorkflowConfig = WorkflowConfig;
//# sourceMappingURL=workflow.model.js.map