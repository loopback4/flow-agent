"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkflowConfigStoreDefinition = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
let WorkflowConfigStoreDefinition = class WorkflowConfigStoreDefinition extends repository_1.Entity {
    name;
    type;
    init_value;
    allow_null;
    required;
    required_for_init;
};
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string'
    }),
    tslib_1.__metadata("design:type", String)
], WorkflowConfigStoreDefinition.prototype, "name", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string'
    }),
    tslib_1.__metadata("design:type", String)
], WorkflowConfigStoreDefinition.prototype, "type", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'any'
    }),
    tslib_1.__metadata("design:type", Object)
], WorkflowConfigStoreDefinition.prototype, "init_value", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'boolean'
    }),
    tslib_1.__metadata("design:type", Boolean)
], WorkflowConfigStoreDefinition.prototype, "allow_null", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'boolean'
    }),
    tslib_1.__metadata("design:type", Boolean)
], WorkflowConfigStoreDefinition.prototype, "required", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'boolean'
    }),
    tslib_1.__metadata("design:type", Boolean)
], WorkflowConfigStoreDefinition.prototype, "required_for_init", void 0);
WorkflowConfigStoreDefinition = tslib_1.__decorate([
    (0, repository_1.model)()
], WorkflowConfigStoreDefinition);
exports.WorkflowConfigStoreDefinition = WorkflowConfigStoreDefinition;
//# sourceMappingURL=workflow.store_definition.model.js.map