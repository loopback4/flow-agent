"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FlowAgentComponent = void 0;
const tslib_1 = require("tslib");
const websocket_1 = require("@loopback4/websocket");
const core_1 = require("@loopback/core");
const models_1 = require("./models");
const repositories_1 = require("./repositories");
const controllers_1 = require("./controllers");
const keys_1 = require("./keys");
const services_1 = require("./services");
// import {repository} from "@loopback/repository";
// import * as _ from 'lodash';
tslib_1.__exportStar(require("./keys"), exports);
let FlowAgentComponent = class FlowAgentComponent {
    application;
    controllers = [controllers_1.FlowController, controllers_1.InstanceController, controllers_1.InstancePortController];
    models = [models_1.Flow, models_1.Instance, models_1.InstancePort];
    // repositories = [FlowRepository, InstanceRepository, InstancePortRepository];
    // providers = {
    //     'flow-agent': LoggerProvider
    // };
    bindings = [
        // @ts-ignore
        core_1.Binding.bind(keys_1.NginxProxyServiceBindings.NGINX_PROXY_SERVICE).toClass(services_1.NginxProxyService),
        core_1.Binding.bind(keys_1.WorkflowProcessesServiceBindings.WORKFLOW_PROCESSES_SERVICE).toClass(services_1.WorkflowProcessesService),
        core_1.Binding.bind(keys_1.RepositoryConstants.FLOW_REPOSITORY).toClass(repositories_1.FlowRepository),
        core_1.Binding.bind(keys_1.RepositoryConstants.INSTANCE_REPOSITORY).toClass(repositories_1.InstanceRepository),
        core_1.Binding.bind(keys_1.RepositoryConstants.INSTANCE_PORT_REPOSITORY).toClass(repositories_1.InstancePortRepository),
    ];
    constructor(
    // @inject(NginxProxyServiceBindings.NGINX_PROXY_SERVICE) private nginxProxyService: NginxProxyService,
    // @repository(FlowRepository) protected flowRepository: FlowRepository,
    // @repository(InstanceRepository) protected instanceRepository: InstanceRepository,
    application) {
        this.application = application;
        application.onStart(async () => {
            const nginxProxy = await application.getBinding(keys_1.NginxProxyServiceBindings.NGINX_PROXY_SERVICE).getValue(application);
            nginxProxy.value();
            const workflowProcesses = await application.getBinding(keys_1.WorkflowProcessesServiceBindings.WORKFLOW_PROCESSES_SERVICE).getValue(application);
            nginxProxy.value();
            //     let instances = _.groupBy(
            //         await this.instanceRepository.find({where: {running: true},include:[{relation: 'ports'}]}),
            //         (instance) => { return instance.flowId }
            //     );
            //     let flowIds = _.keys(instances).map((i) => { return parseInt(i); });
            //     let flows = await this.flowRepository.find({where:{id: {inq: flowIds}}});
            //     console.log('Flows:', flows);
        });
    }
};
FlowAgentComponent = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)(core_1.CoreBindings.APPLICATION_INSTANCE)),
    tslib_1.__metadata("design:paramtypes", [websocket_1.WebsocketApplication])
], FlowAgentComponent);
exports.FlowAgentComponent = FlowAgentComponent;
//# sourceMappingURL=index.js.map