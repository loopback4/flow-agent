"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NginxProxyServiceBindings = exports.RepositoryConstants = exports.WorkflowProcessesServiceBindings = void 0;
const core_1 = require("@loopback/core");
var WorkflowProcessesServiceBindings;
(function (WorkflowProcessesServiceBindings) {
    WorkflowProcessesServiceBindings.WORKFLOW_PROCESSES_SERVICE = core_1.BindingKey.create('services.flow-agent.workflow-processes');
})(WorkflowProcessesServiceBindings = exports.WorkflowProcessesServiceBindings || (exports.WorkflowProcessesServiceBindings = {}));
var RepositoryConstants;
(function (RepositoryConstants) {
    RepositoryConstants.FLOW_REPOSITORY = 'repositories.FlowRepository';
    RepositoryConstants.INSTANCE_REPOSITORY = 'repositories.InstanceRepository';
    RepositoryConstants.INSTANCE_PORT_REPOSITORY = 'repositories.InstancePortRepository';
})(RepositoryConstants = exports.RepositoryConstants || (exports.RepositoryConstants = {}));
var NginxProxyServiceBindings;
(function (NginxProxyServiceBindings) {
    NginxProxyServiceBindings.NGINX_PROXY_SERVICE = core_1.BindingKey.create('services.flow-agent.nginx-proxy');
})(NginxProxyServiceBindings = exports.NginxProxyServiceBindings || (exports.NginxProxyServiceBindings = {}));
//# sourceMappingURL=keys.js.map