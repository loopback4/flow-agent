import { Filter } from '@loopback/repository';
import { Instance } from '../models';
import { InstanceRepository } from '../repositories';
export declare class InstanceController {
    protected instanceRepository: InstanceRepository;
    constructor(instanceRepository: InstanceRepository);
    createInstance(instance: Omit<Instance, 'id'>): Promise<Instance>;
    findInstanceById(id: number, items?: boolean): Promise<Instance | undefined>;
    findInstances(filter?: Filter<Instance>): Promise<Instance[] | undefined>;
    replaceInstance(id: number, instance: Omit<Omit<Instance, 'id'>, 'flowId'>): Promise<void>;
    updateInstance(id: number, instance: Partial<Instance>): Promise<void>;
    deleteInstance(id: number): Promise<void>;
}
