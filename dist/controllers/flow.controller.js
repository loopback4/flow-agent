"use strict";
// Copyright IBM Corp. 2018,2020. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.FlowController = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
let FlowController = class FlowController {
    flowRepository;
    constructor(flowRepository) {
        this.flowRepository = flowRepository;
    }
    async createFlow(flow) {
        const created = await this.flowRepository.create(flow);
        if (created) {
            console.log('Created:', created);
        }
        return created;
    }
    async findFlowById(id, items) {
        return this.flowRepository.findById(id);
    }
    async findFlows(filter) {
        return this.flowRepository.find(filter);
    }
    async replaceFlow(id, flow) {
        if (flow.name) {
            delete flow.name;
        }
        if (flow.requiredGlobals) {
            delete flow.requiredGlobals;
        }
        await this.flowRepository.replaceById(id, flow);
    }
    async updateFlow(id, flow) {
        if (flow.name) {
            delete flow.name;
        }
        if (flow.requiredGlobals) {
            delete flow.requiredGlobals;
        }
        await this.flowRepository.updateById(id, flow);
    }
    async deleteFlow(id) {
        await this.flowRepository.deleteById(id);
    }
};
tslib_1.__decorate([
    (0, rest_1.post)('/flows', {
        responses: {
            '200': {
                description: 'Flow model instance',
                content: { 'application/json': { schema: (0, rest_1.getModelSchemaRef)(models_1.Flow) } },
            },
        },
    }),
    tslib_1.__param(0, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Flow, { title: 'NewFlow', exclude: ['id', 'name', 'requiredGlobals', 'instances'] }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], FlowController.prototype, "createFlow", null);
tslib_1.__decorate([
    (0, rest_1.get)('/flows/{id}', {
        responses: {
            '200': {
                description: 'Flow model instance',
                content: { 'application/json': { schema: (0, rest_1.getModelSchemaRef)(models_1.Flow) } },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, rest_1.param.query.boolean('items')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, Boolean]),
    tslib_1.__metadata("design:returntype", Promise)
], FlowController.prototype, "findFlowById", null);
tslib_1.__decorate([
    (0, rest_1.get)('/flows', {
        responses: {
            '200': {
                description: 'Array of Flow model instances',
                content: {
                    'application/json': {
                        schema: { type: 'array', items: (0, rest_1.getModelSchemaRef)(models_1.Flow) },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.filter(models_1.Flow)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], FlowController.prototype, "findFlows", null);
tslib_1.__decorate([
    (0, rest_1.put)('/flows/{id}', {
        responses: {
            '204': {
                description: 'Flow PUT success',
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Flow, {
                    partial: true,
                    exclude: ['id', 'name', 'requiredGlobals']
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], FlowController.prototype, "replaceFlow", null);
tslib_1.__decorate([
    (0, rest_1.patch)('/flows/{id}', {
        responses: {
            '204': {
                description: 'Flow PATCH success',
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Flow, {
                    partial: true,
                    exclude: ['id', 'name', 'requiredGlobals']
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], FlowController.prototype, "updateFlow", null);
tslib_1.__decorate([
    (0, rest_1.del)('/flows/{id}', {
        responses: {
            '204': {
                description: 'Flow DELETE success',
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number]),
    tslib_1.__metadata("design:returntype", Promise)
], FlowController.prototype, "deleteFlow", null);
FlowController = tslib_1.__decorate([
    tslib_1.__param(0, (0, repository_1.repository)(repositories_1.FlowRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.FlowRepository])
], FlowController);
exports.FlowController = FlowController;
//# sourceMappingURL=flow.controller.js.map