"use strict";
// Copyright IBM Corp. 2018,2020. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.InstanceController = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
let InstanceController = class InstanceController {
    instanceRepository;
    constructor(instanceRepository) {
        this.instanceRepository = instanceRepository;
    }
    async createInstance(instance) {
        try {
            if (instance.managementPort) {
                delete instance.managementPort;
            }
            return await this.instanceRepository.create(instance);
        }
        catch (error) {
            throw new rest_1.HttpErrors.UnprocessableEntity(error.toString());
        }
    }
    async findInstanceById(id, items) {
        return this.instanceRepository.findById(id);
    }
    async findInstances(filter) {
        return this.instanceRepository.find(filter);
    }
    async replaceInstance(id, instance) {
        if (instance.managementPort) {
            delete instance.managementPort;
        }
        await this.instanceRepository.replaceById(id, instance);
    }
    async updateInstance(id, instance) {
        await this.instanceRepository.updateById(id, instance);
    }
    async deleteInstance(id) {
        await this.instanceRepository.deleteById(id);
    }
};
tslib_1.__decorate([
    (0, rest_1.post)('/instances', {
        responses: {
            '200': {
                description: 'Instance model instance',
                content: { 'application/json': { schema: (0, rest_1.getModelSchemaRef)(models_1.Instance) } },
            },
        },
    }),
    tslib_1.__param(0, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Instance, { title: 'NewInstance', exclude: ['id', 'managementPort'] }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], InstanceController.prototype, "createInstance", null);
tslib_1.__decorate([
    (0, rest_1.get)('/instances/{id}', {
        responses: {
            '200': {
                description: 'Instance model instance',
                content: { 'application/json': { schema: (0, rest_1.getModelSchemaRef)(models_1.Instance) } },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, rest_1.param.query.boolean('items')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, Boolean]),
    tslib_1.__metadata("design:returntype", Promise)
], InstanceController.prototype, "findInstanceById", null);
tslib_1.__decorate([
    (0, rest_1.get)('/instances', {
        responses: {
            '200': {
                description: 'Array of Instance model instances',
                content: {
                    'application/json': {
                        schema: { type: 'array', items: (0, rest_1.getModelSchemaRef)(models_1.Instance) },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.filter(models_1.Instance)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], InstanceController.prototype, "findInstances", null);
tslib_1.__decorate([
    (0, rest_1.put)('/instances/{id}', {
        responses: {
            '204': {
                description: 'Instance PUT success',
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Instance, {
                    partial: true,
                    exclude: ['id', 'flowId', 'managementPort']
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], InstanceController.prototype, "replaceInstance", null);
tslib_1.__decorate([
    (0, rest_1.patch)('/instances/{id}', {
        responses: {
            '204': {
                description: 'Instance PATCH success',
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Instance, {
                    partial: true,
                    exclude: ['id', 'flowId', 'managementPort']
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], InstanceController.prototype, "updateInstance", null);
tslib_1.__decorate([
    (0, rest_1.del)('/instances/{id}', {
        responses: {
            '204': {
                description: 'Instance DELETE success',
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number]),
    tslib_1.__metadata("design:returntype", Promise)
], InstanceController.prototype, "deleteInstance", null);
InstanceController = tslib_1.__decorate([
    tslib_1.__param(0, (0, repository_1.repository)(repositories_1.InstanceRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.InstanceRepository])
], InstanceController);
exports.InstanceController = InstanceController;
//# sourceMappingURL=instance.controller.js.map