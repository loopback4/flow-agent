import { Filter } from '@loopback/repository';
import { Flow } from '../models';
import { FlowRepository } from '../repositories';
export declare class FlowController {
    protected flowRepository: FlowRepository;
    constructor(flowRepository: FlowRepository);
    createFlow(flow: Omit<Flow, 'id' | 'name' | 'requiredGlobals' | 'instances'>): Promise<Flow>;
    findFlowById(id: number, items?: boolean): Promise<Flow | undefined>;
    findFlows(filter?: Filter<Flow>): Promise<Flow[] | undefined>;
    replaceFlow(id: number, flow: Omit<Flow, 'id'>): Promise<void>;
    updateFlow(id: number, flow: Partial<Flow>): Promise<void>;
    deleteFlow(id: number): Promise<void>;
}
