"use strict";
// Copyright IBM Corp. 2018,2020. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.InstancePortController = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
let InstancePortController = class InstancePortController {
    portRepository;
    constructor(portRepository) {
        this.portRepository = portRepository;
    }
    async createInstancePort(port) {
        return this.portRepository.create(port);
    }
    async findInstancePortById(id, items) {
        return this.portRepository.findById(id);
    }
    async findInstancePorts(filter) {
        return this.portRepository.find(filter);
    }
    async replaceInstancePort(id, port) {
        await this.portRepository.replaceById(id, port);
    }
    async updateInstancePort(id, port) {
        await this.portRepository.updateById(id, port);
    }
    async deleteInstancePort(id) {
        await this.portRepository.deleteById(id);
    }
};
tslib_1.__decorate([
    (0, rest_1.post)('/ports', {
        responses: {
            '200': {
                description: 'InstancePort model port',
                content: { 'application/json': { schema: (0, rest_1.getModelSchemaRef)(models_1.InstancePort) } },
            },
        },
    }),
    tslib_1.__param(0, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.InstancePort, { title: 'NewInstancePort', exclude: ['id'] }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], InstancePortController.prototype, "createInstancePort", null);
tslib_1.__decorate([
    (0, rest_1.get)('/ports/{id}', {
        responses: {
            '200': {
                description: 'InstancePort model port',
                content: { 'application/json': { schema: (0, rest_1.getModelSchemaRef)(models_1.InstancePort) } },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, rest_1.param.query.boolean('items')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, Boolean]),
    tslib_1.__metadata("design:returntype", Promise)
], InstancePortController.prototype, "findInstancePortById", null);
tslib_1.__decorate([
    (0, rest_1.get)('/ports', {
        responses: {
            '200': {
                description: 'Array of InstancePort model ports',
                content: {
                    'application/json': {
                        schema: { type: 'array', items: (0, rest_1.getModelSchemaRef)(models_1.InstancePort) },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.filter(models_1.InstancePort)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], InstancePortController.prototype, "findInstancePorts", null);
tslib_1.__decorate([
    (0, rest_1.put)('/ports/{id}', {
        responses: {
            '204': {
                description: 'InstancePort PUT success',
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, models_1.InstancePort]),
    tslib_1.__metadata("design:returntype", Promise)
], InstancePortController.prototype, "replaceInstancePort", null);
tslib_1.__decorate([
    (0, rest_1.patch)('/ports/{id}', {
        responses: {
            '204': {
                description: 'InstancePort PATCH success',
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.InstancePort, { partial: true }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], InstancePortController.prototype, "updateInstancePort", null);
tslib_1.__decorate([
    (0, rest_1.del)('/ports/{id}', {
        responses: {
            '204': {
                description: 'InstancePort DELETE success',
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number]),
    tslib_1.__metadata("design:returntype", Promise)
], InstancePortController.prototype, "deleteInstancePort", null);
InstancePortController = tslib_1.__decorate([
    tslib_1.__param(0, (0, repository_1.repository)(repositories_1.InstancePortRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.InstancePortRepository])
], InstancePortController);
exports.InstancePortController = InstancePortController;
//# sourceMappingURL=instance-port.controller.js.map