import { Filter } from '@loopback/repository';
import { InstancePort } from '../models';
import { InstancePortRepository } from '../repositories';
export declare class InstancePortController {
    protected portRepository: InstancePortRepository;
    constructor(portRepository: InstancePortRepository);
    createInstancePort(port: Omit<InstancePort, 'id'>): Promise<InstancePort>;
    findInstancePortById(id: number, items?: boolean): Promise<InstancePort | undefined>;
    findInstancePorts(filter?: Filter<InstancePort>): Promise<InstancePort[] | undefined>;
    replaceInstancePort(id: number, port: InstancePort): Promise<void>;
    updateInstancePort(id: number, port: Partial<InstancePort>): Promise<void>;
    deleteInstancePort(id: number): Promise<void>;
}
