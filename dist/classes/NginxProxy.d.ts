import { WebsocketApplication } from "@loopback4/websocket";
import { Flow, Instance, InstancePort } from "../models";
import { DataObject } from "@loopback/repository";
export declare class NginxProxy {
    constructor();
    init(app: WebsocketApplication): Promise<void>;
    createFlow(flow: Flow): Promise<void>;
    updateFlowById(id: typeof Flow.prototype.id, data: DataObject<Flow>): Promise<void>;
    replaceFlowById(id: typeof Flow.prototype.id, data: DataObject<Flow>): Promise<void>;
    deleteFlow(flow: Flow): Promise<void>;
    deleteFlowById(id: typeof Flow.prototype.id): Promise<void>;
    createInstance(instance: Instance): Promise<void>;
    updateInstanceById(id: typeof Instance.prototype.id, data: DataObject<Instance>): Promise<void>;
    replaceInstanceById(id: typeof Instance.prototype.id, data: DataObject<Instance>): Promise<void>;
    deleteInstance(instance: Instance): Promise<void>;
    deleteInstanceById(id: typeof Instance.prototype.id): Promise<void>;
    createInstancePort(instancePort: InstancePort): Promise<void>;
    updateInstancePortById(id: typeof InstancePort.prototype.id, data: DataObject<InstancePort>): Promise<void>;
    replaceInstancePortById(id: typeof InstancePort.prototype.id, data: DataObject<InstancePort>): Promise<void>;
    deleteInstancePort(instancePort: InstancePort): Promise<void>;
    deleteInstancePortById(id: typeof InstancePort.prototype.id): Promise<void>;
}
