"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FreePorts = void 0;
const tslib_1 = require("tslib");
const _ = tslib_1.__importStar(require("lodash"));
const dgram = tslib_1.__importStar(require("dgram"));
const net = tslib_1.__importStar(require("net"));
const minPort = 1024;
const maxPort = 65535;
class FreePorts {
    options;
    // private interfaces: NodeJS.Dict<os.NetworkInterfaceInfo[]>;
    // private addresses: Set<undefined | string> = new Set([undefined, '0.0.0.0']);
    host;
    ports = _.range(minPort, maxPort);
    excludedTcpPorts = [];
    excludedUdpPorts = [];
    constructor(options) {
        this.options = options;
        this.update(options);
    }
    update(options) {
        if (options) {
            this.host = options.host;
            if (options.excludeTcp && !Array.isArray(options.excludeTcp)) {
                this.excludedTcpPorts = [options.excludeTcp];
            }
            else if (options.excludeTcp && Array.isArray(options.excludeTcp)) {
                this.excludedTcpPorts = options.excludeTcp;
            }
            if (options.excludeUdp && !Array.isArray(options.excludeUdp)) {
                this.excludedUdpPorts = [options.excludeUdp];
            }
            else if (options.excludeUdp && Array.isArray(options.excludeUdp)) {
                this.excludedUdpPorts = options.excludeUdp;
            }
            if (options.ports && !Array.isArray(options.ports)) {
                this.ports = [options.ports];
            }
            else if (options.ports && Array.isArray(options.ports)) {
                this.ports = options.ports;
            }
            else {
                this.ports = _.range(minPort, maxPort);
            }
        }
        else {
            this.ports = _.range(minPort, maxPort);
        }
        // this.interfaces = os.networkInterfaces();
        // this.getAddresses();
    }
    async get(protocol = "tcp", host) {
        for (const port of this.ports) {
            if (!_.includes(this.excludedTcpPorts, port) && await this.checkPort(protocol, port, host)) {
                return port;
            }
        }
        return undefined;
    }
    // private getAddresses() {
    //     // Undefined value for createServer to use default host
    //     // default IPv4 host in case createServer default to IPv6
    //     const results = new Set([undefined, '0.0.0.0']);
    //     for (const iface of Object.values(this.interfaces)) {
    //         if (iface) {
    //             for (const config of iface) {
    //                 results.add(config.address);
    //             }
    //         }
    //     }
    //     this.addresses = results;
    //     return results;
    // }
    async checkPort(protocol, port, host) {
        if (protocol === 'tcp') {
            return new Promise((resolve, reject) => {
                const server = net.createServer();
                server.unref();
                server.on('error', reject);
                server.listen(port, host, () => {
                    server.close(() => {
                        resolve(port);
                    });
                });
            });
        }
        else if (protocol === 'udp') {
            return new Promise((resolve, reject) => {
                const socket = dgram.createSocket('udp4');
                socket.on('error', reject);
                socket.bind(port, host, () => {
                    socket.close();
                    resolve(port);
                });
            });
        }
        else {
            return undefined;
        }
    }
}
exports.FreePorts = FreePorts;
//# sourceMappingURL=FreePorts.js.map