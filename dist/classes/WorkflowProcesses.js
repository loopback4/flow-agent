"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkflowProcesses = void 0;
const tslib_1 = require("tslib");
const config_loader_1 = require("@loopback4/config-loader");
const keys_1 = require("../keys");
const _ = tslib_1.__importStar(require("lodash"));
class WorkflowProcesses {
    constructor() {
    }
    async init(app) {
        console.log('Workflow Processes Initialized');
        const loader = await app.getBinding(config_loader_1.ConfigLoaderBindings.CONFIG_LOADER).getValue(app);
        const config = loader.config;
        const flowRepository = await app.getBinding(keys_1.RepositoryConstants.FLOW_REPOSITORY).getValue(app);
        const instanceRepository = await app.getBinding(keys_1.RepositoryConstants.INSTANCE_REPOSITORY).getValue(app);
        for (const instance of await instanceRepository.find({ where: { persist: false } })) {
            await instanceRepository.deleteById(instance.id);
        }
        // instances = await instanceRepository.find({where: {running: true}});
        let groupedInstances = _.groupBy(await instanceRepository.find({ where: { running: true }, include: [{ relation: 'ports' }] }), (instance) => { return instance.flowId; });
        let flowIds = _.keys(groupedInstances).map((i) => { return parseInt(i); });
        let flows = await flowRepository.find({ where: { id: { inq: flowIds } } });
        for (const flow of flows) {
            if (flow.id) {
                let flowInstances = groupedInstances[flow.id];
                for (const instance of flowInstances) {
                    // TODO Handle processing of workflow processes
                }
            }
        }
    }
    // Flow intercept calls
    async createFlow(flow) {
    }
    async updateFlowById(id, data) {
    }
    async replaceFlowById(id, data) {
    }
    async deleteFlow(flow) {
    }
    async deleteFlowById(id) {
    }
    // Instance intercept calls
    async createInstance(instance) {
    }
    async updateInstanceById(id, data) {
    }
    async replaceInstanceById(id, data) {
    }
    async deleteInstance(instance) {
    }
    async deleteInstanceById(id) {
    }
    // InstancePort intercept calls
    async createInstancePort(instancePort) {
    }
    async updateInstancePortById(id, data) {
    }
    async replaceInstancePortById(id, data) {
    }
    async deleteInstancePort(instancePort) {
    }
    async deleteInstancePortById(id) {
    }
}
exports.WorkflowProcesses = WorkflowProcesses;
//# sourceMappingURL=WorkflowProcesses.js.map