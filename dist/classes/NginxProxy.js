"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NginxProxy = void 0;
const keys_1 = require("../keys");
const config_loader_1 = require("@loopback4/config-loader");
const logger_1 = require("@loopback4/logger");
class NginxProxy {
    constructor() {
    }
    async init(app) {
        console.log('Nginx Proxy Initialized');
        const logger = await app.getBinding(logger_1.LoggerServiceBindings.LOGGER_SERVICE).getValue(app);
        console.log(logger);
        const loader = await app.getBinding(config_loader_1.ConfigLoaderBindings.CONFIG_LOADER).getValue(app);
        const config = loader.config;
        console.log('Config', config);
        const instanceRepository = await app.getBinding(keys_1.RepositoryConstants.INSTANCE_REPOSITORY).getValue(app);
        for (const instance of await instanceRepository.find({ where: { persist: false } })) {
            console.log(`Deleting non-persistent instance(${instance.id})`);
            await instanceRepository.deleteById(instance.id);
        }
        for (const instance of await instanceRepository.find({ where: { running: true }, include: [{ relation: 'ports' }] })) {
            // TODO Handle generating nginx config and reloading
        }
    }
    // Flow intercept calls
    async createFlow(flow) {
    }
    async updateFlowById(id, data) {
    }
    async replaceFlowById(id, data) {
    }
    async deleteFlow(flow) {
    }
    async deleteFlowById(id) {
    }
    // Instance intercept calls
    async createInstance(instance) {
    }
    async updateInstanceById(id, data) {
    }
    async replaceInstanceById(id, data) {
    }
    async deleteInstance(instance) {
    }
    async deleteInstanceById(id) {
    }
    // InstancePort intercept calls
    async createInstancePort(instancePort) {
    }
    async updateInstancePortById(id, data) {
    }
    async replaceInstancePortById(id, data) {
    }
    async deleteInstancePort(instancePort) {
    }
    async deleteInstancePortById(id) {
    }
}
exports.NginxProxy = NginxProxy;
//# sourceMappingURL=NginxProxy.js.map