export interface FreePortsOptions {
    ports?: number | number[];
    excludeTcp?: number | number[];
    excludeUdp?: number | number[];
    host?: string;
}
export declare type Protocol = "tcp" | "udp";
export declare class FreePorts {
    private options?;
    host?: string;
    ports: number[];
    excludedTcpPorts?: number[];
    excludedUdpPorts?: number[];
    constructor(options?: FreePortsOptions | undefined);
    update(options?: FreePortsOptions): void;
    get(protocol?: Protocol, host?: string): Promise<number | undefined>;
    checkPort(protocol: Protocol, port: number, host?: string): Promise<unknown>;
}
