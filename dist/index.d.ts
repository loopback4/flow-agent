import { WebsocketApplication } from "@loopback4/websocket";
import { Component, Binding } from '@loopback/core';
import { Flow, Instance, InstancePort } from "./models";
import { FlowController, InstanceController, InstancePortController } from "./controllers";
export * from './keys';
export declare class FlowAgentComponent implements Component {
    private application;
    controllers: (typeof FlowController | typeof InstanceController | typeof InstancePortController)[];
    models: (typeof Flow | typeof Instance | typeof InstancePort)[];
    bindings: Binding[];
    constructor(application: WebsocketApplication);
}
