import { BindingKey } from '@loopback/core';
import { WorkflowProcessesService } from "./services";
export declare namespace WorkflowProcessesServiceBindings {
    const WORKFLOW_PROCESSES_SERVICE: BindingKey<WorkflowProcessesService>;
}
export declare namespace RepositoryConstants {
    const FLOW_REPOSITORY = "repositories.FlowRepository";
    const INSTANCE_REPOSITORY = "repositories.InstanceRepository";
    const INSTANCE_PORT_REPOSITORY = "repositories.InstancePortRepository";
}
export declare namespace NginxProxyServiceBindings {
    const NGINX_PROXY_SERVICE: BindingKey<WorkflowProcessesService>;
}
