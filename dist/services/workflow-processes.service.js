"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkflowProcessesService = void 0;
const tslib_1 = require("tslib");
const classes_1 = require("../classes");
const core_1 = require("@loopback/core");
const websocket_1 = require("@loopback4/websocket");
const workflowProcesses = new classes_1.WorkflowProcesses();
let WorkflowProcessesService = class WorkflowProcessesService {
    application;
    initialized = false;
    constructor(application) {
        this.application = application;
    }
    async value() {
        if (!this.initialized) {
            await workflowProcesses.init(this.application);
        }
        return workflowProcesses;
    }
};
WorkflowProcessesService = tslib_1.__decorate([
    (0, core_1.injectable)({ scope: core_1.BindingScope.SINGLETON }),
    tslib_1.__param(0, (0, core_1.inject)(core_1.CoreBindings.APPLICATION_INSTANCE)),
    tslib_1.__metadata("design:paramtypes", [websocket_1.WebsocketApplication])
], WorkflowProcessesService);
exports.WorkflowProcessesService = WorkflowProcessesService;
//# sourceMappingURL=workflow-processes.service.js.map