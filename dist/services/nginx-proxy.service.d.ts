import { Provider } from '@loopback/context';
import { NginxProxy } from "../classes";
import { WebsocketApplication } from "@loopback4/websocket";
export declare class NginxProxyService implements Provider<NginxProxy> {
    private application;
    private initialized;
    constructor(application: WebsocketApplication);
    value(): Promise<NginxProxy>;
}
