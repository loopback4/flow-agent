"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NginxProxyService = void 0;
const tslib_1 = require("tslib");
const classes_1 = require("../classes");
const core_1 = require("@loopback/core");
const websocket_1 = require("@loopback4/websocket");
const nginxProxy = new classes_1.NginxProxy();
let NginxProxyService = class NginxProxyService {
    application;
    initialized = false;
    constructor(application) {
        this.application = application;
    }
    async value() {
        if (!this.initialized) {
            await nginxProxy.init(this.application);
        }
        return nginxProxy;
    }
};
NginxProxyService = tslib_1.__decorate([
    (0, core_1.injectable)({ scope: core_1.BindingScope.SINGLETON }),
    tslib_1.__param(0, (0, core_1.inject)(core_1.CoreBindings.APPLICATION_INSTANCE)),
    tslib_1.__metadata("design:paramtypes", [websocket_1.WebsocketApplication])
], NginxProxyService);
exports.NginxProxyService = NginxProxyService;
//# sourceMappingURL=nginx-proxy.service.js.map