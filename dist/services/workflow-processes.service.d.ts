import { Provider } from '@loopback/context';
import { WorkflowProcesses } from "../classes";
import { WebsocketApplication } from "@loopback4/websocket";
export declare class WorkflowProcessesService implements Provider<WorkflowProcesses> {
    private application;
    private initialized;
    constructor(application: WebsocketApplication);
    value(): Promise<WorkflowProcesses>;
}
