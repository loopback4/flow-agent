import { Getter } from '@loopback/core';
import { DefaultCrudRepository, juggler, HasManyRepositoryFactory, DataObject, Options } from '@loopback/repository';
import { InstanceRepository } from '../repositories';
import { Flow, FlowRelations, Instance } from '../models';
import { NginxProxy, WorkflowProcesses } from "../classes";
export declare class FlowRepository extends DefaultCrudRepository<Flow, typeof Flow.prototype.id, FlowRelations> {
    private nginxProxy;
    private workflowProcesses;
    protected instanceRepositoryGetter: Getter<InstanceRepository>;
    readonly instances: HasManyRepositoryFactory<Instance, typeof Flow.prototype.id>;
    constructor(dataSource: juggler.DataSource, nginxProxy: NginxProxy, workflowProcesses: WorkflowProcesses, instanceRepositoryGetter: Getter<InstanceRepository>);
    create(entity: DataObject<Partial<Flow>>, options?: Options): Promise<Flow>;
    processUpdateData(data: DataObject<Flow>): void;
    updateById(id: typeof Flow.prototype.id, data: DataObject<Flow>, options?: Options): Promise<void>;
    replaceById(id: typeof Flow.prototype.id, data: DataObject<Flow>, options?: Options): Promise<void>;
    delete(entity: Flow, options?: any): Promise<void>;
    deleteById(id: typeof Flow.prototype.id, options?: any): Promise<void>;
}
