"use strict";
// Copyright IBM Corp. 2018,2019. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.FlowRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const models_1 = require("../models");
const keys_1 = require("../keys");
const classes_1 = require("../classes");
let FlowRepository = class FlowRepository extends repository_1.DefaultCrudRepository {
    nginxProxy;
    workflowProcesses;
    instanceRepositoryGetter;
    instances;
    constructor(dataSource, nginxProxy, workflowProcesses, instanceRepositoryGetter) {
        super(models_1.Flow, dataSource);
        this.nginxProxy = nginxProxy;
        this.workflowProcesses = workflowProcesses;
        this.instanceRepositoryGetter = instanceRepositoryGetter;
        this.instances = this.createHasManyRepositoryFactoryFor('instances', instanceRepositoryGetter);
        this.registerInclusionResolver('instances', this.instances.inclusionResolver);
    }
    async create(entity, options) {
        entity.requiredGlobals = [];
        if (entity?.config?.store_definition) {
            for (const storeDef of entity.config.store_definition) {
                if (storeDef?.required_for_init && storeDef?.name) {
                    entity.requiredGlobals.push(storeDef.name);
                }
            }
        }
        if (!entity.name) {
            entity.name = entity.config?.name;
        }
        const flow = await super.create(entity, options);
        await this.nginxProxy.createFlow(flow);
        await this.workflowProcesses.createFlow(flow);
        return flow;
    }
    processUpdateData(data) {
        if (data.config) {
            data.requiredGlobals = [];
            if (!data.name) {
                data.name = data.config.name;
            }
            if (data?.config?.store_definition) {
                for (const storeDef of data.config.store_definition) {
                    if (storeDef?.required_for_init && storeDef?.name) {
                        data.requiredGlobals.push(storeDef.name);
                    }
                }
            }
        }
    }
    async updateById(id, data, options) {
        this.processUpdateData(data);
        await this.nginxProxy.updateFlowById(id, data);
        return super.updateById(id, data, options);
    }
    async replaceById(id, data, options) {
        this.processUpdateData(data);
        await this.nginxProxy.replaceFlowById(id, data);
        await this.workflowProcesses.replaceFlowById(id, data);
        return super.replaceById(id, data, options);
    }
    async delete(entity, options) {
        await this.nginxProxy.deleteFlow(entity);
        await this.workflowProcesses.deleteFlow(entity);
        await this.instances(entity.id).delete();
        return super.delete(entity, options);
    }
    async deleteById(id, options) {
        await this.nginxProxy.deleteFlowById(id);
        await this.workflowProcesses.deleteFlowById(id);
        await this.instances(id).delete({});
        return super.deleteById(id, options);
    }
};
FlowRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)('datasources.db')),
    tslib_1.__param(1, (0, core_1.inject)(keys_1.NginxProxyServiceBindings.NGINX_PROXY_SERVICE)),
    tslib_1.__param(2, (0, core_1.inject)(keys_1.WorkflowProcessesServiceBindings.WORKFLOW_PROCESSES_SERVICE)),
    tslib_1.__param(3, repository_1.repository.getter('InstanceRepository')),
    tslib_1.__metadata("design:paramtypes", [repository_1.juggler.DataSource, classes_1.NginxProxy,
        classes_1.WorkflowProcesses, Function])
], FlowRepository);
exports.FlowRepository = FlowRepository;
//# sourceMappingURL=flow.repository.js.map