"use strict";
// Copyright IBM Corp. 2018,2019. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.InstanceRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const repositories_1 = require("../repositories");
const models_1 = require("../models");
const _ = tslib_1.__importStar(require("lodash"));
const keys_1 = require("../keys");
const classes_1 = require("../classes");
const classes_2 = require("../classes");
// import getPort, {portNumbers} from 'get-port';
// const getPort = await import('get-port');
const config_loader_1 = require("@loopback4/config-loader");
const configProvider_1 = require("@loopback4/config-loader/dist/configProvider");
let InstanceRepository = class InstanceRepository extends repository_1.DefaultCrudRepository {
    configLoader;
    nginxProxy;
    workflowProcesses;
    instancePortRepositoryGetter;
    flowRepositoryGetter;
    flowRepository;
    ports;
    flow;
    freePorts = new classes_2.FreePorts({});
    constructor(dataSource, configLoader, nginxProxy, workflowProcesses, instancePortRepositoryGetter, flowRepositoryGetter, flowRepository) {
        super(models_1.Instance, dataSource);
        this.configLoader = configLoader;
        this.nginxProxy = nginxProxy;
        this.workflowProcesses = workflowProcesses;
        this.instancePortRepositoryGetter = instancePortRepositoryGetter;
        this.flowRepositoryGetter = flowRepositoryGetter;
        this.flowRepository = flowRepository;
        this.ports = this.createHasManyRepositoryFactoryFor('ports', instancePortRepositoryGetter);
        this.registerInclusionResolver('ports', this.ports.inclusionResolver);
        this.flow = this.createBelongsToAccessorFor('flow', flowRepositoryGetter);
        this.registerInclusionResolver('flow', this.flow.inclusionResolver);
    }
    checkGlobals(flow, globals) {
        let missing = [];
        if (flow.requiredGlobals) {
            for (const reqGlob of flow.requiredGlobals) {
                if (globals && globals[reqGlob] === undefined) {
                    missing.push(reqGlob);
                }
            }
        }
        if (missing.length !== 0) {
            return missing;
        }
        return true;
    }
    async getManagementPorts() {
        return (await this.find({ fields: { managementPort: true } })).map((instance) => { return instance.managementPort; });
    }
    async create(entity, options) {
        const flow = await this.flowRepository.findById(entity.flowId);
        const check = this.checkGlobals(flow, entity.globals);
        if (check === true) {
            const config = this.configLoader.config;
            console.log('Flow Agent Config', config.flowAgent);
            // entity.managementPort = await getPort.default({port: getPort.portNumbers(config.flowAgent.managementPortStart, config.flowAgent.managementPortEnd), exclude: await this.getManagementPorts()});
            this.freePorts.update({
                excludeTcp: await this.getManagementPorts(),
                ports: _.range(config.flowAgent.managementPortStart, config.flowAgent.managementPortEnd),
            });
            entity.managementPort = await this.freePorts.get('tcp');
            const instance = await super.create(entity, options);
            await this.nginxProxy.createInstance(instance);
            await this.workflowProcesses.createInstance(instance);
            return instance;
        }
        else {
            throw new Error(`Missing Globals(${_.join(check, ',')})`);
        }
    }
    async updateById(id, data, options) {
        if (data.globals) {
            const instance = await this.findById(id);
            const flow = await this.flowRepository.findById(instance.flowId);
            const check = this.checkGlobals(flow, data.globals);
            if (check !== true) {
                throw new Error(`Missing Globals(${_.join(check, ',')})`);
            }
        }
        await this.nginxProxy.updateInstanceById(id, data);
        await this.workflowProcesses.updateInstanceById(id, data);
        return super.updateById(id, data, options);
    }
    async replaceById(id, data, options) {
        if (data.globals) {
            const instance = await this.findById(id);
            const flow = await this.flowRepository.findById(instance.flowId);
            const check = this.checkGlobals(flow, data.globals);
            if (check !== true) {
                throw new Error(`Missing Globals(${_.join(check, ',')})`);
            }
        }
        await this.nginxProxy.replaceInstanceById(id, data);
        await this.workflowProcesses.replaceInstanceById(id, data);
        return super.replaceById(id, data, options);
    }
    async delete(entity, options) {
        await this.nginxProxy.deleteInstance(entity);
        await this.workflowProcesses.deleteInstance(entity);
        await this.ports(entity.id).delete({});
        return super.delete(entity, options);
    }
    async deleteById(id, options) {
        await this.nginxProxy.deleteInstanceById(id);
        await this.workflowProcesses.deleteInstanceById(id);
        await this.ports(id).delete({});
        return super.deleteById(id, options);
    }
};
InstanceRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)('datasources.db')),
    tslib_1.__param(1, (0, core_1.inject)(config_loader_1.ConfigLoaderBindings.CONFIG_LOADER)),
    tslib_1.__param(2, (0, core_1.inject)(keys_1.NginxProxyServiceBindings.NGINX_PROXY_SERVICE)),
    tslib_1.__param(3, (0, core_1.inject)(keys_1.WorkflowProcessesServiceBindings.WORKFLOW_PROCESSES_SERVICE)),
    tslib_1.__param(4, repository_1.repository.getter('InstancePortRepository')),
    tslib_1.__param(5, repository_1.repository.getter('FlowRepository')),
    tslib_1.__param(6, (0, repository_1.repository)(repositories_1.FlowRepository)),
    tslib_1.__metadata("design:paramtypes", [repository_1.juggler.DataSource, configProvider_1.ConfigProvider,
        classes_1.NginxProxy,
        classes_1.WorkflowProcesses, Function, Function, repositories_1.FlowRepository])
], InstanceRepository);
exports.InstanceRepository = InstanceRepository;
//# sourceMappingURL=instance.repository.js.map