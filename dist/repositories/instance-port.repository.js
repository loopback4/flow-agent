"use strict";
// Copyright IBM Corp. 2018,2019. All Rights Reserved.
// Node module: @loopback/example-todo
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.InstancePortRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const models_1 = require("../models");
const keys_1 = require("../keys");
const classes_1 = require("../classes");
let InstancePortRepository = class InstancePortRepository extends repository_1.DefaultCrudRepository {
    nginxProxy;
    workflowProcesses;
    instanceRepositoryGetter;
    instance;
    constructor(dataSource, nginxProxy, workflowProcesses, instanceRepositoryGetter) {
        super(models_1.InstancePort, dataSource);
        this.nginxProxy = nginxProxy;
        this.workflowProcesses = workflowProcesses;
        this.instanceRepositoryGetter = instanceRepositoryGetter;
        this.instance = this.createBelongsToAccessorFor('instance', instanceRepositoryGetter);
        this.registerInclusionResolver('instance', this.instance.inclusionResolver);
    }
    async create(entity, options) {
        const instancePort = await super.create(entity, options);
        await this.nginxProxy.createInstancePort(instancePort);
        await this.workflowProcesses.createInstancePort(instancePort);
        return instancePort;
    }
    async updateById(id, data, options) {
        await this.nginxProxy.updateInstancePortById(id, data);
        await this.workflowProcesses.updateInstancePortById(id, data);
        return super.updateById(id, data, options);
    }
    async replaceById(id, data, options) {
        await this.nginxProxy.replaceInstancePortById(id, data);
        await this.workflowProcesses.replaceInstancePortById(id, data);
        return super.replaceById(id, data, options);
    }
    async delete(entity, options) {
        await this.nginxProxy.deleteInstancePort(entity);
        await this.workflowProcesses.deleteInstancePort(entity);
        return super.delete(entity, options);
    }
    async deleteById(id, options) {
        await this.nginxProxy.deleteInstancePortById(id);
        await this.workflowProcesses.deleteInstancePortById(id);
        return super.deleteById(id, options);
    }
};
InstancePortRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)('datasources.db')),
    tslib_1.__param(1, (0, core_1.inject)(keys_1.NginxProxyServiceBindings.NGINX_PROXY_SERVICE)),
    tslib_1.__param(2, (0, core_1.inject)(keys_1.WorkflowProcessesServiceBindings.WORKFLOW_PROCESSES_SERVICE)),
    tslib_1.__param(3, repository_1.repository.getter('InstanceRepository')),
    tslib_1.__metadata("design:paramtypes", [repository_1.juggler.DataSource, classes_1.NginxProxy,
        classes_1.WorkflowProcesses, Function])
], InstancePortRepository);
exports.InstancePortRepository = InstancePortRepository;
//# sourceMappingURL=instance-port.repository.js.map