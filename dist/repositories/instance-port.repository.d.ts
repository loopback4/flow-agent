import { Getter } from '@loopback/core';
import { BelongsToAccessor, DefaultCrudRepository, juggler, DataObject, Options } from '@loopback/repository';
import { InstancePort, InstancePortRelations, InstanceWithRelations } from '../models';
import { InstanceRepository } from "../repositories";
import { NginxProxy, WorkflowProcesses } from "../classes";
export declare class InstancePortRepository extends DefaultCrudRepository<InstancePort, typeof InstancePort.prototype.id, InstancePortRelations> {
    private nginxProxy;
    private workflowProcesses;
    protected instanceRepositoryGetter: Getter<InstanceRepository>;
    readonly instance: BelongsToAccessor<InstanceWithRelations, typeof InstancePort.prototype.id>;
    constructor(dataSource: juggler.DataSource, nginxProxy: NginxProxy, workflowProcesses: WorkflowProcesses, instanceRepositoryGetter: Getter<InstanceRepository>);
    create(entity: DataObject<InstancePort>, options?: Options): Promise<InstancePort>;
    updateById(id: typeof InstancePort.prototype.id, data: DataObject<InstancePort>, options?: Options): Promise<void>;
    replaceById(id: typeof InstancePort.prototype.id, data: DataObject<InstancePort>, options?: Options): Promise<void>;
    delete(entity: InstancePort, options?: Options): Promise<void>;
    deleteById(id: typeof InstancePort.prototype.id, options?: Options): Promise<void>;
}
