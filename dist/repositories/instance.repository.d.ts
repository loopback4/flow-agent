import { Getter } from '@loopback/core';
import { DefaultCrudRepository, BelongsToAccessor, HasManyRepositoryFactory, juggler, DataObject, Options } from '@loopback/repository';
import { FlowRepository, InstancePortRepository } from '../repositories';
import { Flow, Instance, InstancePortWithRelations, InstanceRelations } from '../models';
import { NginxProxy, WorkflowProcesses } from "../classes";
import { ConfigProvider } from "@loopback4/config-loader/dist/configProvider";
export declare class InstanceRepository extends DefaultCrudRepository<Instance, typeof Instance.prototype.id, InstanceRelations> {
    private configLoader;
    private nginxProxy;
    private workflowProcesses;
    protected instancePortRepositoryGetter: Getter<InstancePortRepository>;
    protected flowRepositoryGetter: Getter<FlowRepository>;
    protected flowRepository: FlowRepository;
    readonly ports: HasManyRepositoryFactory<InstancePortWithRelations, typeof Instance.prototype.id>;
    readonly flow: BelongsToAccessor<Flow, typeof Instance.prototype.id>;
    private freePorts;
    constructor(dataSource: juggler.DataSource, configLoader: ConfigProvider, nginxProxy: NginxProxy, workflowProcesses: WorkflowProcesses, instancePortRepositoryGetter: Getter<InstancePortRepository>, flowRepositoryGetter: Getter<FlowRepository>, flowRepository: FlowRepository);
    checkGlobals(flow: Flow, globals: any): true | string[];
    getManagementPorts(): Promise<number[]>;
    create(entity: DataObject<Instance>, options?: Options): Promise<Instance>;
    updateById(id: typeof Instance.prototype.id, data: DataObject<Instance>, options?: Options): Promise<void>;
    replaceById(id: typeof Instance.prototype.id, data: DataObject<Instance>, options?: Options): Promise<void>;
    delete(entity: Instance, options?: any): Promise<void>;
    deleteById(id: typeof Instance.prototype.id, options?: any): Promise<void>;
}
